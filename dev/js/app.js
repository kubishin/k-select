import Vue from "vue"
import component from "../vue-component.vue"

new Vue({
  el: '#app',
  render: h => h(component)
})
